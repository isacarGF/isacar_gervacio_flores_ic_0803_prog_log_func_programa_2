/*
     Interfaz Funcional
*/

public interface CalculadoraInt {
    public int calcular(long x, long y);
}
